var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var pug = require('gulp-pug');
var changed = require('gulp-changed');

var thePumbler = function (task) {
  return plumber({
    errorHandler: function (err) {
      notify.onError({
        title: task + " error in " + err.plugin,
        message: err.toString()
      })(err);
    }
  });
}

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'pug'], function () {

  browserSync.init({
    server: "./app",
    notify: false,
    browser: 'google chrome'
  });

  gulp.watch("app/scss/*.scss", ['sass']);
  gulp.watch('app/pug/*.pug', ['pug']);
  gulp.watch("app/*.html").on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function () {
  return gulp.src("app/scss/style.scss")
    .pipe(thePumbler('sass'))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(gulp.dest("app/css"))
    .pipe(browserSync.stream());
});

gulp.task('pug', function () {
  return gulp.src('app/pug/*.pug')
    .pipe(thePumbler('pug'))
    .pipe(changed('app', {extension: '.html'}))
    .pipe(pug({pretty: true}))
    .pipe(gulp.dest('app'))
});

gulp.task('default', ['serve']);