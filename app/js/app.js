var currentYear = 1950
var currentPage = 'home'
var currentAlbum = ''
var playerLoaded = false
var playing = false
var oldPlayer, player, volume

var popup = {
  init: function() {
    this.bind()
  },
  element: function() {
    return document.getElementsByClassName('js-trigger-popup')
  },
  bind: function() {
    [].forEach.call(this.element(), function(element) {
      element.addEventListener('click', function(e){
        e.preventDefault()
        this.toggle()
      }.bind(this))
    }.bind(this))
  },
  isOpen: function() {
    return document.body.classList.contains('js-popup--show')
  },
  toggle: function() {
    document.body.classList.toggle('js-popup--show')
    if(!this.loaded) {
      this.loaded = true
    }
  }
}

var homeSlider = {
  init: function() {
    this.slider = tns({
      container: '.js-tns-home',
      items: 5
    })

    this.slider.events.on('indexChanged', this.activeSlide)
    this.slider.events.on('transitionEnd', this.triggerChangeTimeline)

    this.activeSlide()
  },
  activeSlide: function() {
    var slide = document.querySelectorAll('.tns-slide-active');
    [].forEach.call(slide, function(element, index) {
      element.classList.remove('js-tns-active')
    })
    slide[2].classList.add('js-tns-active')
  },
  triggerChangeTimeline: function() {
    var slide = document.querySelector('.js-tns-home').querySelectorAll('.tns-slide-active');
    var year = slide[2].getAttribute('data-year')
    changeTimeline.change(year)
  },
  geserSlide: function(id) {
    var slide = document.querySelectorAll('.tns-slide-active');
    [].forEach.call(slide, function(element, index) {
      if(slide[index] == id.parentElement) {
        switch(index) {
          case 0:
            this.slider.goTo('prev');
            this.slider.goTo('prev');
          break;
          case 1:
            this.slider.goTo('prev');
          break;
          case 3:
            this.slider.goTo('next');
          break;
          case 4:
            this.slider.goTo('next');
            this.slider.goTo('next');
          break;
        }
      }
    }.bind(this))
  },
  rebuild: function() {
    this.slider.rebuild()
  },
  load: function() {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', './data/albums.json')
    xhr.send(null)

    xhr.onreadystatechange = function () {
      var DONE = 4;
      var OK = 200;
      if (xhr.readyState === DONE) {
        if (xhr.status === OK) {
          this.injectHtml(xhr.responseText)
        } else {
          window.alert('Error: ' + xhr.status)
        }
      }
    }.bind(this)
  },
  injectHtml: function(response) {
    var html = "";
    var json = JSON.parse(response)
    json.data.forEach(function(item, index){
      html += '<li class="main-slider__item" data-year="'+item.released+'" data-index="'+index+'"><a class="main-slider__item-link js-trigger-page" data-page="album" data-album="'+item.id+'"><img class="main-slider__item-cover" src="'+item.album.cover+'"></a><a class="main-slider__item-link js-trigger-page" data-page="album"><span class="main-slider__item-title">'+item.album.name+'</span><span class="main-slider__item-author">'+item.album.artist+'</span></a></li>'
    })

    var slider = document.querySelector('.js-tns-home')
    slider.innerHTML = html

    this.init()
    changePage.init()
    albumSlider.load(json.data)
  }
}

var changeTimeline = {
  init: function() {
    this.bind()
  },
  element: function() {
    return document.querySelectorAll('.js-trigger-timeline')
  },
  bind: function() {
    [].forEach.call(this.element(), function(element) {
      element.addEventListener('click', function(e) {
        e.preventDefault()
        this.change(e.target.getAttribute('data-year'), true)
      }.bind(this));
    }.bind(this))
  },
  change: function(year, slide) {
    currentYear = year;
    var timeline = document.querySelectorAll('.js-timeline');
    [].forEach.call(timeline, function(element) {
      element.setAttribute('data-year', year)
    }.bind(this))

    var yearHeadline = document.querySelectorAll('.main-headline__year');
    [].forEach.call(yearHeadline, function(element) {
      element.innerHTML = year + '\'s'
    })

    // geser Slide
    if(slide) {
      var indexHome = document.querySelector('.js-tns-home').querySelector('.main-slider__item[data-year="'+year+'"]').getAttribute('data-index')
      homeSlider.slider.goTo(indexHome)

      var indexAlbum = document.querySelector('.js-tns-album').querySelector('.main-album__more-item[data-year="'+year+'"]').getAttribute('data-index')
      albumSlider.slider.goTo(indexAlbum)
    }
  }
}

var changePage = {
  init: function() {
    var element = document.querySelectorAll('.js-trigger-page');
    [].forEach.call(element, function(element) {
      if(element.getAttribute('data-loaded') === null) {
        element.setAttribute('data-loaded', 'true')
        element.addEventListener('click', function(e) {
          e.preventDefault()
          if(element.parentElement.classList.contains('tns-slide-active') && element.parentElement.classList.contains('main-slider__item') && !element.parentElement.classList.contains('js-tns-active')) {
            homeSlider.geserSlide(element)
          } else {
            var page = element.getAttribute('data-page')
            this.change(page)
          }
          if(page == 'album') {
            this.loadAlbum(element.getAttribute('data-album'))
          }
        }.bind(this))
      }
    }.bind(this))
  },
  change: function(page) {
    document.querySelector('#' + currentPage + '-page').style.display = 'none'

    currentPage = page

    document.querySelector('#' + currentPage + '-page').style.display = 'block'

    if(page === 'album') {
      window.scrollTo(0,0)
    }
  },
  loadAlbum: function(id_album) {
    currentAlbum = id_album
    dummyAlbum = (currentAlbum%2)===1?2:1
    // ganti url dengan real api, contoh: blalabla.com/album/{id_album}
    var xhr = new XMLHttpRequest()
    xhr.open('GET', './data/tracklist-'+dummyAlbum+'.json') // JANGAN LUPA GANTI URL DENGAN API BENERAN
    xhr.send(null)

    xhr.onreadystatechange = function () {
      var DONE = 4;
      var OK = 200;
      if (xhr.readyState === DONE) {
        if (xhr.status === OK) {
          this.injectHtml(xhr.responseText)
        } else {
          window.alert('Error: ' + xhr.status)
        }
      }
    }.bind(this)
  },
  injectHtml: function(response) {
    var html = ''
    var json = JSON.parse(response)
    json.data.tracklist.forEach(function(item, index) {
      html += '<li class="main-song__playlist-item js-trigger-trackchange" data-song="'+item.title+'" data-artist="'+item.artist+'">'+
        '<div class="main-song__playlist-icon"></div>'+
        '<div class="main-song__playlist-track">'+item.track+'</div>'+
        '<div class="main-song__playlist-song js-marquee-song"><span>'+item.title+'</span></div>'+
        '<div class="main-song__playlist-singer js-marquee-artist"><span>'+item.artist+'</span></div>'+
      '</li>'
    })

    // load player
    if(!playerLoaded) {
      window.loadPlayer(json.data.tracklist)
    } else {
      oldPlayer = player
    }

    // tracklist
    var wrapper = document.querySelector('.js-playlist')
    wrapper.innerHTML = html

    //album info
    var albumCover = document.querySelectorAll('.js-album-cover');
    [].forEach.call(albumCover, function(element) {
      element.setAttribute('src', json.data.cover)
    })

    var albumName = document.querySelector('.js-album-name')
    albumName.innerHTML = json.data.name

    var albumArtist = document.querySelector('.js-album-artist')
    albumArtist.innerHTML = json.data.artist

    var albumLabel = document.querySelector('.js-album-label')
    albumLabel.innerHTML = json.data.label

    var albumDescription= document.querySelector('.js-album-description')
    albumDescription.innerHTML = json.data.description

    var albumFormat = document.querySelector('.js-album-format')
    albumFormat.innerHTML = json.data.format

    var trackList = document.querySelectorAll('.js-trigger-trackchange');
    [].forEach.call(trackList, function(element, index) {
      if(element.getAttribute('data-loaded') === null) {
        element.setAttribute('data-loaded', 'true')
        element.addEventListener('click', function(e) {
          e.preventDefault()

          if (oldPlayer && oldPlayer.isPlaying()) {
            oldPlayer.pause()
            oldPlayer = null
            window.loadPlayer(json.data.tracklist)
          }

          setTimeout(function(){
            player.volume(volume)
            player.skipTo(index)
          }, 100);

          [].forEach.call(albumCover, function(element) {
            element.classList.remove('hide')
          })

          var curSong = element.getAttribute('data-song')
          var curArtist = element.getAttribute('data-artist')
          changePage.changeNowPlaying(curSong, curArtist)
          playerLoaded = true
        }.bind(this))
      }
    }.bind(this))
  },
  changeNowPlaying: function(title, artist) {
    var npSong = document.querySelector('.js-np-song')
    var npArtist = document.querySelector('.js-np-artist')
    npSong.innerHTML = title
    npArtist.innerHTML = artist

    var current = document.querySelector('.main-song__playlist-item--current')
    if(current) {
      var wSong = current.querySelectorAll('marquee')[0]
      if(wSong) {
        current.innerHTML = current.innerHTML.replace('<marquee', '<span')
      }

      var wArtist = current.querySelectorAll('marquee')[1]
      if(wArtist) {
        curretn.innerHTML = current.innerHTML.replace('<marquee', '<span')
      }

      current.classList.remove('main-song__playlist-item--current')
    }

    var element = document.querySelector('[data-song="'+title+'"][data-artist="'+artist+'"]')

    var mSong = element.querySelector('.js-marquee-song')
    if(mSong) { 
      var wSong = mSong.querySelector('span')
      if(wSong.offsetWidth > 450) {
        mSong.innerHTML = mSong.innerHTML.replace('<span', '<marquee')
      }
    }
    var mArtist = element.querySelector('.js-marquee-artist')
    if(mArtist) {
      var wArtist = mArtist.querySelector('span')
      if(wArtist && wArtist.offsetWidth > 250) {
        mArtist.innerHTML = mArtist.innerHTML.replace('<span', '<marquee')
      }
    }

    element.classList.add('main-song__playlist-item--current')
  }
}

var albumSlider = {
  init: function() {
    this.slider = tns({
      container: '.js-tns-album',
      items: 10,
      slideby: 'page',
      loop: false
    })

    this.slider.events.on('transitionEnd', this.triggerChangeTimeline)
  },
  triggerChangeTimeline: function() {
    var slide = document.querySelector('.js-tns-album').querySelectorAll('.tns-slide-active');
    var year = slide[9].getAttribute('data-year')
    changeTimeline.change(year)
  },
  load: function(data) {
    var html = ""
    data.forEach(function(item, index) {
      html += '<li class="main-album__more-item" data-year="'+item.released+'" data-index="'+index+'"><a class="main-album__more-link js-trigger-page" data-page="album" data-album="'+item.id+'"><img class="main-album__more-cover" src="'+item.album.cover+'"></a></li>'
    })

    var element = document.querySelector('.js-tns-album')
    element.innerHTML = html

    this.init()
    changePage.init()
  },
  rebuild: function() {
    this.slider.rebuild()
  }
}

var moreAlbum = {
  load: function(year) {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', './data/albums.json')
    xhr.send(null)

    xhr.onreadystatechange = function () {
      var DONE = 4;
      var OK = 200;
      if (xhr.readyState === DONE) {
        if (xhr.status === OK) {
          this.injectHtml(xhr.responseText)
        } else {
          window.alert('Error: ' + xhr.status)
        }
      }
    }.bind(this)
  },
  injectHtml: function(response) {
    var html = "";
    var json = JSON.parse(response)
    json.data.forEach(function(item){
      html += '<li class="main-album__item" data-year="'+item.released+'"><a class="main-album__item-link"><img class="main-album__item-cover" src="'+item.album.cover+'"><span class="main-album__item-note">Only playable on website</span></a><a class="main-album__item-link"><span class="main-album__item-title">'+item.album.name+'</span><span class="main-album__item-author">'+item.album.artist+'</span></li>'
    })

    var wrapper = document.querySelector('.js-more-album')
    wrapper.innerHTML = html

    changePage.init()
  }
}

var volumeSlider = {
  init: function() {
    this.bindClick()
    this.bindInput()
  },
  bindClick: function() {
    var element = document.querySelector('.js-volume-toggle')
    element.addEventListener('click', function(e){
      e.preventDefault()
      e.target.nextElementSibling.classList.toggle('show')
    })
  },
  bindInput: function() {
    var full = document.querySelector('.js-volume-full')
    var slider = document.querySelector('.js-volume-slider')
    slider.oninput = function() {
      var width = ((10 - this.value) * 20)
      if (width > 0) { width += 20 }
      full.style.width =  width + 'px'

      volume = (10 - this.value) / 10
      if (player) { player.volume(volume) }
    }
  }
}

var disableRightClick = {
  init: function() {
    document.addEventListener('contextmenu', event => event.preventDefault());
  }
}

var disableMultiTouch = {
  init: function() {
    document.addEventListener('touchstart', function(event) {
      if(event.touches.length > 1){
        //the event is multi-touch
        //you can then prevent the behavior
        event.preventDefault()
      }
    })
  }
}

var inactivityTime = {
  init: function() {
    var t;
    var minutes = 5;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer;  // catches touchscreen presses as well      
    window.ontouchstart = resetTimer; // catches touchscreen swipes as well 
    window.onclick = resetTimer;      // catches touchpad clicks as well
    window.onkeypress = resetTimer;   
    window.addEventListener('scroll', resetTimer, true); // improved; see comments

    function yourFunction() {
      window.location.href = './intro.html'
    }

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(yourFunction, (minutes * 60000));
    }
  }
}

// initialize
document.addEventListener("DOMContentLoaded", function() {
  popup.init()
  changeTimeline.init()
  changePage.init()
  volumeSlider.init()
  disableRightClick.init()
  disableMultiTouch.init()
  inactivityTime.init()

  homeSlider.load()
  moreAlbum.load(1950)
});